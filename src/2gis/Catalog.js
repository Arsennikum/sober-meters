import config from "./config";

class Catalog {

    static makeRequest(searchKeywordsArray, regionId, bounds, success, fail) {
        let result = [];
        let loadedCount = 0;
        searchKeywordsArray.forEach(function(query, i, array) {
            const url = Catalog.buildUrlForSearchByQuery(query, bounds, regionId);

            const xhr = new (("onload" in new XMLHttpRequest()) ? XMLHttpRequest : XDomainRequest);
            xhr.open('GET', url, true);

            xhr.onload = function() {
                const response = JSON.parse(xhr.responseText);

                if ((response.result === undefined) || (response.result.items === undefined)) {
                    fail(response);
                    return;
                }

                // объединяю массивы
                result = response.result.items.reduce(function(coll,item){
                    coll.push( item );
                    return coll;
                }, result);

                if (++loadedCount === array.length) {
                    success(result);
                }
            };

            xhr.onerror = fail;

            xhr.send();
        });
    }

    static buildUrlForSearchByQuery(query, bounds, regionId) {

        return `https://catalog.api.2gis.ru/3.0/markers?` +
            `viewpoint1=${bounds.getNorthWest().lng},${bounds.getNorthWest().lat}` +
            `&viewpoint2=${bounds.getSouthEast().lng},${bounds.getSouthEast().lat}` +
            `&page=1&page_size=${config.markersPageSize}` +
            `&q=${query}` +
            `&region_id=${regionId}` +
            `&key=${config.key}`;
    }

    static buildUrlForGetInfo(id, bounds, regionId) {

        const fields = ['items.org', 'items.address', 'items.name_ex', 'items.point'
            // ,'items.reviews','items.external_content','search_attributes','items.stop_factors','items.region_id','items.adm_div' -- unused
        ];

        return `https://catalog.api.2gis.ru/2.0/catalog/branch/get?` +
            `id=${id}` +
            //`&see_also_size=0` +
            `&region_id=${regionId}` +
            `&viewpoint1=${bounds.getNorthWest().lng},${bounds.getNorthWest().lat}` +
            `&viewpoint2=${bounds.getSouthEast().lng},${bounds.getSouthEast().lat}` +
            `&fields=${fields}` +
            `&key=${config.key}`;
    }

    static detectRegionId(lon, lat, success, fail) {
        const url = `https://catalog.api.2gis.ru/2.0/region/search?` +
            `q=${lon},${lat}` +
            `&key=${config.key}`;

        let xhr = new (("onload" in new XMLHttpRequest()) ? XMLHttpRequest : XDomainRequest);
        xhr.open('GET', url, true);

        xhr.onload = function() {
            const response = JSON.parse(xhr.responseText);

            if ((response.result === undefined) || (response.result.items === undefined)) {
                fail(response);
                return;
            }
            success(response.result.items[0].id);
        };
        xhr.onerror = fail;

        xhr.send();
    }

    static buildGetInfoPromise(catalogItemId, bounds, regionId) {
        return new Promise(function (success, fail) {
            const url = Catalog.buildUrlForGetInfo(catalogItemId, bounds, regionId);

            const xhr = new XMLHttpRequest();
            xhr.open('GET', url);
            xhr.onload = function () {
                const response = JSON.parse(xhr.responseText);

                if ((response.result === undefined) || (response.result.items === undefined)) {
                    fail(response);
                    return;
                }
                success(response.result.items);
            };
            xhr.onerror = fail;
            xhr.send();
        });
    }
}

export default Catalog;