class IconsFactory {

    static getRedIcon() {
        return this.getIconWithColor('red');
    }

    static getYellowIcon() {
        return this.getIconWithColor('yellow');
    }

    static getGreenIcon() {
        return this.getIconWithColor('green');

    }

    static getSelectedIcon() {
        return this.getIconWithColor('violet');
    }

    // Use carefully outside this class.
    // Better way - make specific method in this class
    static getIconWithColor(color) {
        return new L.Icon({
            iconUrl: `https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-${color}.png`,
            shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
            iconSize: [25, 41],
            iconAnchor: [12, 41],
            popupAnchor: [1, -34],
            shadowSize: [41, 41]
        });
    }
}

export default IconsFactory;