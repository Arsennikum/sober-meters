;

import config from "./config.js";
import "../styles/style.css";
import IconsFactory from "./IconsFactory";
import Catalog from "./Catalog";

let map,
    /** Array of catalog items of shops */
    shopsArray,
    /** Array of catalog items of education */
    educationArray,
    oldCenter,
    regionId = 0,
    coord = [57.096083, 65.623548],
    selectedSet = new Set(),
    selectedElementsGroup;

// DG.then происходит, когда будет подгружен api карт
DG.then(function () {
    initMap();

    selectedElementsGroup = DG.featureGroup();

    oldCenter = map.getCenter();
    initRaidRoute();

    map.on('moveend' , function() {
        if (map.getZoom() <= config.minZoom) {
            return;
        }
        if (map.getCenter().distanceTo(oldCenter) > config.refreshDistance) {
            oldCenter = map.getCenter();
            redrawShops(map.getBounds());
            redrawEducation(map.getBounds());
        }
    });
    map.on('zoomend', function() {
        let bounds = map.getBounds();

        if (map.getZoom() > config.minZoom) {
            redrawShops(bounds);
            redrawEducation(bounds);
            return;
        }

        if (map.getZoom() <= config.regionZoom) {
            return;
        }

        Catalog.detectRegionId(
            bounds.getCenter().lng,
            bounds.getCenter().lat,
            function(detectedRegion) {
                if (detectedRegion == regionId) {
                    return;
                }

                regionId = detectedRegion;
                fillAllData();
            });

    })
});

function initMap() {
    map = DG.map('map', {
        center: coord,
        zoom: 10,
        geoclicker: true
    });
    DG.control.ruler().addTo(map);
}

function initRaidRoute() {
    const paramMarkers = getParameterByName('items');
    if (paramMarkers) {
        JSON.parse(paramMarkers).forEach(function (itemId, i, array) {
            selectedSet.add(itemId);
            Catalog.buildGetInfoPromise(itemId, map.getBounds(), regionId)
                .then(function (items) {
                    var marker = DG.marker([items[0].point.lat, items[0].point.lon]);
                    marker.addTo(selectedElementsGroup);
                    bindLoadInfoForPopupEvent(marker, itemId); // TODO: там дублируется запрос
                })
        });
        selectedElementsGroup.addTo(map);
    }
}

// При загрузке страницы
document.addEventListener("DOMContentLoaded", function(){
    document.getElementById("btnMain").onclick = function() {

        const a = document.createElement('a');
        const linkText = document.createTextNode(config.localization.linkName);
        a.appendChild(linkText);
        a.href = '?items=' + encodeURIComponent(JSON.stringify([...selectedSet]));

        const raidRouteDiv = document.getElementById('raidRoute');
        raidRouteDiv.appendChild(document.createElement('br'));
        raidRouteDiv.appendChild(a);

        /*
        if (map.hasLayer(redrawShops.currentLayerGroup)){
            redrawShops.currentLayerGroup.remove();
            redrawEducation.currentLayerGroup.remove();
        } else {
            redrawShops.currentLayerGroup.addTo(map);
            redrawEducation.currentLayerGroup.addTo(map);
        }*/
    }
});

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, '\\$&');
    let regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

/**
 * Отрисовать маркеры {@link layerMarkers}, попадающие в указанную область
 * @param bounds - область, в которой отрисовать маркеры
 */
function redrawShops(bounds) {
    if (redrawShops.currentLayerGroup !== undefined) {
        redrawShops.currentLayerGroup.remove();
    }

    redrawShops.currentLayerGroup = DG.layerGroup();
    shopsArray.forEach(function(catalogItem, i, array) {
        if (bounds.contains(DG.latLng(catalogItem.lat, catalogItem.lon))) {
            if (! catalogItem.marker) {
                catalogItem.marker = buildMarker(catalogItem);
            }
            catalogItem.marker.addTo(redrawShops.currentLayerGroup);
        }
    });
    redrawShops.currentLayerGroup.addTo(map);
}

/**
 * Отрисовать окружности, попадающие в указанную область
 * @param bounds - область, в которой отрисовать окружности
 */
function redrawEducation(bounds) {
    if (redrawEducation.currentLayerGroup !== undefined) {
        redrawEducation.currentLayerGroup.remove();
    }

    redrawEducation.currentLayerGroup = DG.layerGroup();
    educationArray.forEach(function(catalogItem, i, array) {
        if (bounds.contains(DG.latLng(catalogItem.lat, catalogItem.lon))) {
            buildCircle(catalogItem).addTo(redrawEducation.currentLayerGroup);
        }
    });
    redrawEducation.currentLayerGroup.addTo(map);
}

function setColorTag() {

    shopsArray.forEach(function(shop, i, array) {

        if (shop.selected || shop.colorTag) {
            return;
        }

        for (let education of educationArray) {

            const distance = DG.latLng(education.lat, education.lon)
                .distanceTo(DG.latLng(shop.lat, shop.lon));

            if (distance < config.circleRadius) {
                shop.colorTag = 'red';
                break;
            }
            if (distance < config.middleDistance) {
                shop.colorTag = 'yellow';
            }
        }
    });
}

function fillAllData() {
    let shopsSuccess = false,
        educationSuccess = false,
        onSuccess = function () {
            if (shopsSuccess && educationSuccess) {
                setColorTag();
            }
        };

    new Promise(findShops).then(function(items) {
        shopsArray = items;
        shopsSuccess = true;
        onSuccess();
        checkIfSelected();
    });

    new Promise(findEducation).then(function(items) {
        educationArray = items;
        educationSuccess = true;
        onSuccess();
    });
}

function checkIfSelected() {
    shopsArray.forEach(function (shop, i, array) {
        selectedSet.forEach(selectedId => {
            let shopId = shop.id.split("_")[0].trim();
            if (shopId === selectedId.trim()) {
                shop.selected = true;
            }
        });
    });
}

function findShops(success, fail) {
    Catalog.makeRequest(config.shopSearchQueries, regionId, map.getBounds(), success, fail);
}

function findEducation(success, fail) {
    Catalog.makeRequest(config.educationSearchQueries, regionId, map.getBounds(), success, fail);
}

function buildMarker(catalogItem) {
    let markerOptions;
    if (catalogItem.selected) {
        markerOptions = {icon: IconsFactory.getSelectedIcon()}
    } else if (catalogItem.colorTag == 'red') {
        markerOptions = {icon: IconsFactory.getRedIcon()};
    } else if (catalogItem.colorTag == 'yellow') {
        markerOptions = {icon: IconsFactory.getYellowIcon()};
    } else {
        markerOptions = {icon: IconsFactory.getGreenIcon()};
    }
    const marker = DG.marker([catalogItem.lat, catalogItem.lon], markerOptions);

    bindLoadInfoForPopupEvent(marker, catalogItem.id);
    bindSelectEvent(catalogItem.id, marker, markerOptions.icon);

    return marker;
}

function buildCircle(catalogItem) {
    const circle = DG.circle([catalogItem.lat, catalogItem.lon], config.circleRadius);

    bindLoadInfoForPopupEvent(circle, catalogItem.id);

    return circle;
}

function bindSelectEvent(itemId, element, elementIcon) {
    element.on(config.markerSelectEvent, function (event) {
        if (element.selected) {
            element.selected = false;
            element.setIcon(elementIcon);

            selectedSet.delete(itemId);
        } else {
            element.selected = true;
            element.setIcon(IconsFactory.getSelectedIcon());

            selectedSet.add(itemId.split("_")[0]);
        }
    });
}

function bindLoadInfoForPopupEvent(element, catalogItemId) {
    element.on('mouseover', function(event) {
        if (element.getPopup() !== undefined) {
            return;
        }

        const getInfo = Catalog.buildGetInfoPromise(catalogItemId, map.getBounds(), regionId);

        getInfo.then(function(items) {
            const popupContent = items[0].address_name
                + (items[0].address_comment !== undefined ? ' (' + items[0].address_comment + ')' : '')
                + '. <br/>' + items[0].name;
            element.bindPopup(popupContent);
        });
    });
}