import "../styles/style.css";

function init() {
    var coords, Collect; // Переменная для коллекции геообъектов.
    var myMap = new ymaps.Map('map', {
            center: [55.734046, 37.588628],
            zoom: 4
    });
    
    ymaps.geocode('Тюмень', {results:1}).then(function(res){
        myMap.setCenter(res.geoObjects.get(0).geometry.getCoordinates(),11);
    });
    
    myMap.events.add('boundschange', OnChange);
    
function OnChange(event)
{
    if (event.get('newZoom')<13) return;
    Collect = new ymaps.GeoObjectCollection();
    search(true, ["образование","школы","детские сады"]);
    search(false, ['продукты','универсамы','супермаркеты']);

function search(circle, ar)
{
    var draw;
    if (circle) draw = DrawCircle; else draw = DrawPoint;
    myMap.geoObjects.removeAll(); // Очищаем предыдущее
    ar.forEach(function(item){
            ymaps.search(
            item,
            {
                results: 3000,
                boundedBy: event.get('newBounds') //myMap.getBounds()
            }
            ).then(function (result){
                // result.geoObjects — коллекция геообъектов с результатами поиска.
                    result.geoObjects.each(draw);
                });
        });
    myMap.geoObjects.add(Collect); // Добавляем всё
}
}

function DrawCircle(res)
{
    var myCircle = new ymaps.Circle([res.geometry.getCoordinates(), 100],
    {
        hintContent: res.properties.get("name"),
		balloonContent: res.properties.get("address")
    });
    Collect.add(myCircle);
}

function DrawPoint(res)
{
    var Mark = new ymaps.Placemark(res.geometry.getCoordinates(), {
        hintContent: res.properties.get("name"),
		balloonContent: res.properties.get("address")
		});
    Collect.add(Mark);
}

}

ymaps.ready(init);